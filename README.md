# core.mod

This repo will contain the core modules for my fork of blitzmax
Splitting the modules from the compiler and IDE etc. (the "distribution")
will allow for updating the two parts independently of each other.

core.mod will contain most essential classes and lower level
implementations. These will be wrapped in more easy to use
functions etc in brl.mod.
